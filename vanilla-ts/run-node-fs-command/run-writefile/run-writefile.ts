/**
 * Runs the writefile Node FS command
 */

import { isEmpty, defaultsDeep, toString } from "lodash";
import { writeFile } from "node:fs/promises";

/**
 * Defines the function interface
 */
export interface TRunWritefile {
  /**
   * A file to write to.
   */
  file?: string;
  /**
   * The content to write to the file.
   */
  content?: string;
  /**
   * Whether to log or not
   */
  isVerbose?: boolean;
}

/**
 * Defines the function default props
 */
export const runWritefileDefaultProps: TRunWritefile = {
  file: "",
  content: "",
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - null: invalid or missing props
 * - undefined: success, or at least no error
 * - void: one of writeFile's return values
 * - Error: error
 */
export type TRunWritefileReturnType = null | undefined | void | Error;

/**
 * Defines the function body
 */
export async function runWritefile(
  props?: TRunWritefile
): Promise<TRunWritefileReturnType> {
  const propsMerged = defaultsDeep(props, runWritefileDefaultProps);
  const { file, content, isVerbose } = propsMerged;

  /**
   * Logs the input data, if isVerbose is true
   */
  if (isVerbose) {
    console.log("Running writeFile:", propsMerged);
  }

  /**
   * Fails safely, with invalid or missing props
   */
  if (isEmpty(file)) return null;

  /**
   * Runs the writeFile command
   */
  let result = undefined;

  try {
    /**
     * Returns undefined, if success
     * See https://nodejs.org/api/fs.html#fspromiseswritefilefile-data-options
     */
    result = await writeFile(file, content, "utf8");
  } catch (error: unknown) {
    result = new Error(toString(error));
  }

  /**
   * Logs the result, if isVerbose is true
   */
  if (isVerbose) {
    console.log("Result of writeFile:", result);
  }

  return result;
}
