import {
  runWritefile,
  TRunWritefile,
  TRunWritefileReturnType,
} from "./run-writefile";
import { runRmrf } from "../run-rmrf";

/**
 * Input and output data for testing
 */
const validInputData: TRunWritefile = {
  file: ".test.writeFile.txt",
  content: "Hello, world!",
};
const validOutputData: TRunWritefileReturnType = undefined;
const invalidInputData: TRunWritefile = {
  file: "non-existent-folder/test.txt",
};

/**
 * Clean up after the test cases
 */
afterEach(async () => {
  await runRmrf({ folder: ".test.writeFile.txt" });
});

/**
 * The test cases
 */
it("Fails safely, with an error", async () => {
  const result = await runWritefile(invalidInputData);
  expect(result).toBeInstanceOf(Error);
});

it("Works, Returns undefined, if success", async () => {
  const result = await runWritefile(validInputData);
  expect(result).toEqual(validOutputData);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runWritefile({ ...validInputData, isVerbose: true });
  expect(mockLog).toHaveBeenCalled();

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runWritefile();
  expect(result).toBeNull();
});
