import { runRmrf, TRunRmrf, TRunRmrfReturnType } from "./run-rmrf";
import { runMkdir } from "../run-mkdir";

/**
 * Input and output data for testing
 */
const validInputData: TRunRmrf = {
  folder: ".test",
};
const validOutputData: TRunRmrfReturnType = undefined;
const invalidInputData: TRunRmrf = {
  folder: "non-existent-folder",
};

/**
 * Clean up after the test cases
 */
afterEach(async () => {
  await runRmrf({ folder: ".test" });
});

/**
 * The test cases
 */
it("Fails safely, with an error", async () => {
  const result = await runRmrf(invalidInputData);
  expect(result).toBeInstanceOf(Error);
});

it("Works, Returns undefined, if success", async () => {
  await runMkdir(validInputData);
  const result = await runRmrf(validInputData);
  expect(result).toEqual(validOutputData);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runRmrf({ ...validInputData, isVerbose: true });
  expect(mockLog).toHaveBeenCalled();

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runRmrf();
  expect(result).toBeNull();
});
