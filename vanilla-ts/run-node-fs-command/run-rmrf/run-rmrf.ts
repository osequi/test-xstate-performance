/**
 * Runs the rm Node FS command
 */

import { isEmpty, defaultsDeep, toString } from "lodash";
import { rm } from "fs/promises";

/**
 * Defines the function interface
 */
export interface TRunRmrf {
  /**
   * A folder **OR** a file to remove.
   */
  folder?: string;
  /**
   * Options for the rm command
   */
  options?: Record<string, unknown>;
  /**
   * Whether to log or not
   */
  isVerbose?: boolean;
}

/**
 * Defines the function default props
 */
export const runRmrfDefaultProps: TRunRmrf = {
  folder: "",
  options: {
    recursive: true,
  },
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - null: invalid or missing props
 * - undefined: success, or at least no error
 * - void: one of the rm return types
 * - Error: error
 */
export type TRunRmrfReturnType = null | undefined | void | Error;

/**
 * Defines the function body
 */
export async function runRmrf(props?: TRunRmrf): Promise<TRunRmrfReturnType> {
  const propsMerged = defaultsDeep(props, runRmrfDefaultProps);
  const { folder, options, isVerbose } = propsMerged;

  /**
   * Logs the input data, if isVerbose is true
   */
  if (isVerbose) {
    console.log("Running rmRf:", propsMerged);
  }

  /**
   * Fails safely, with invalid or missing props
   */
  if (isEmpty(folder)) return null;

  /**
   * Runs the rm command
   */
  let result = undefined;

  try {
    /**
     * Returns undefined, if success
     * See https://nodejs.org/api/fs.html#fspromisesrmpath-options
     */
    result = await rm(folder, options);
  } catch (error: unknown) {
    result = new Error(toString(error));
  }

  /**
   * Logs the output data, if isVerbose is true
   */
  if (isVerbose) {
    console.log("Result of running rmRf:", result);
  }

  return result;
}
