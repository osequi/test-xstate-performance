import {
  runReadfile,
  TRunReadfile,
  TRunReadfileReturnType,
} from "./run-readfile";

/**
 * Input and output data for testing
 */
const validInputData: TRunReadfile = {
  file: "package.json",
};
const validOutputData: TRunReadfileReturnType = "string";
const invalidInputData: TRunReadfile = {
  file: "non-existent-file.json",
};

/**
 * The test cases
 */
it("Fails safely, with an error", async () => {
  const result = await runReadfile(invalidInputData);
  expect(result).toBeInstanceOf(Error);
});

it("Works, Returns string - the content of the file - if success", async () => {
  const result = await runReadfile(validInputData);
  expect(typeof result).toBe(validOutputData);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runReadfile({ ...validInputData, isVerbose: true });
  expect(mockLog).toHaveBeenCalled();

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runReadfile();
  expect(result).toBeNull();
});
