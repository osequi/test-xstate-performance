/**
 * Runs the readFile Node FS command
 */

import { isEmpty, defaultsDeep, toString } from "lodash";
import { readFile } from "node:fs/promises";

/**
 * Defines the function interface
 */
export interface TRunReadfile {
  /**
   * The file to read
   */
  file?: string;
  /**
   * Whether to log the input and output data
   */
  isVerbose?: boolean;
}

/**
 * Defines the function default props
 */
export const runReadfileDefaultProps: TRunReadfile = {
  file: "",
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - null: invalid or missing props
 * - undefined: success, or at least no error
 * - string: success, the content of the file
 * - Error: error
 */
export type TRunReadfileReturnType = null | undefined | string | Error;

/**
 * Defines the function body
 */
export async function runReadfile(
  props?: TRunReadfile
): Promise<TRunReadfileReturnType> {
  const propsMerged = defaultsDeep(props, runReadfileDefaultProps);
  const { file, isVerbose } = propsMerged;

  /**
   * Logs the input data, if isVerbose is true
   */
  if (isVerbose) {
    console.log("Running readFile:", propsMerged);
  }

  /**
   * Fails safely, with invalid or missing props
   */
  if (isEmpty(file)) return null;

  /**
   * Runs the readFile command
   */
  let result = undefined;

  try {
    /**
     * Returns string - the content of the file - if success
     * See https://nodejs.org/api/fs.html#fspromisesreadfilepath-options
     */
    result = await readFile(file, "utf8");
  } catch (error: unknown) {
    result = new Error(toString(error));
  }

  return result;
}
