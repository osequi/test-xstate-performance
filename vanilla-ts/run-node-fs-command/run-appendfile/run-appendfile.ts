/**
 * Runs the appendFile Node FS  command
 */

import { isEmpty, defaultsDeep, toString } from "lodash";
import { appendFile } from "node:fs/promises";

/**
 * Defines the function interface
 */
export interface TRunAppendfile {
  /**
   * The file to append
   */
  file?: string;
  /**
   * The content to add
   */
  content?: string;
  /**
   * Logging verbosity
   */
  isVerbose?: boolean;
}

/**
 * Defines the function default props
 */
export const runAppendfileDefaultProps: TRunAppendfile = {
  file: "",
  content: "",
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - null: invalid or missing props
 * - undefined: initial and/or success state, with valid props
 * - void: the appendFile call result
 * - Error: error
 */
export type TRunAppendfileReturnType = null | undefined | void | Error;

/**
 * Defines the function body
 */
export async function runAppendfile(
  props?: TRunAppendfile
): Promise<TRunAppendfileReturnType> {
  const propsMerged = defaultsDeep(props, runAppendfileDefaultProps);
  const { file, content, isVerbose } = propsMerged;

  /**
   * Logs the input props, when isVerbose is true
   */
  if (isVerbose) {
    console.log("Running appendFile:", propsMerged);
  }

  /**
   * Fails safely, with invalid or missing props
   */
  if (isEmpty(file)) return null;

  /**
   * Appends the file
   * - When the file does not exist, it is created
   */
  let result = undefined;

  try {
    /**
     * No documentation on the return value of appendFile
     * https://nodejs.org/api/fs.html#fs_fspromises_appendfile_path_data_options
     */
    result = await appendFile(file, content, "utf8");
    /**
     * Here error: Nodejs.ErrnoException is not a valid type
     * It should be unknown
     * See https://stackoverflow.com/questions/69021040/why-catch-clause-variable-type-annotation-must-be-any
     */
  } catch (error: unknown) {
    /**
     * Fails safely, with an error
     */
    result = new Error(toString(error));
  }

  /**
   * Logs the result, when isVerbose is true
   */
  if (isVerbose) {
    console.log("Result of running appendFile:", result);
  }

  return result;
}
