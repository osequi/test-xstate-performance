import {
  runAppendfile,
  TRunAppendfile,
  TRunAppendfileReturnType,
} from "./run-appendfile";
import { runRmrf } from "../run-rmrf/run-rmrf";

/**
 * Input and output data for testing
 */
const validInputData: TRunAppendfile = {
  file: ".test.append.txt",
  content: "Hello world!",
};
const validOutputData: TRunAppendfileReturnType = undefined;
const validInputDataVerbose: TRunAppendfile = {
  file: ".test.append.txt",
  content: "Hello world!",
  isVerbose: true,
};
const invalidInputData: TRunAppendfile = {
  file: "I have a piece of code that I need to test. One of the test requires to add coverage where a stream.Writable is created giving a specified output file name. I need to test the case where the Writable should fail, that it should emit the error event and the proper error handling function should execute, etc. etc.",
};

/**
 * Clean up after the test cases
 */
afterEach(async () => {
  await runRmrf({ folder: ".test.append.txt" });
});

/**
 * The test cases
 */
it("Works, Creates and writes, or, appends the file", async () => {
  const result = await runAppendfile(validInputData);
  expect(result).toEqual(validOutputData);
});

it("Fails safely, with an error", async () => {
  const result = await runAppendfile(invalidInputData);
  expect(result).toBeInstanceOf(Error);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runAppendfile(validInputDataVerbose);
  expect(mockLog).toHaveBeenCalledTimes(2);

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runAppendfile();
  expect(result).toBeNull();
});
