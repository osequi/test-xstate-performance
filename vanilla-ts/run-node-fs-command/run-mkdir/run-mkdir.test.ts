import { runMkdir, TRunMkdir, TRunMkdirReturnType } from "./run-mkdir";
import { runRmrf } from "../";

/**
 * Input and output data for testing
 */
const validInputData: TRunMkdir = {
  folder: ".test",
};
const validOutputData: TRunMkdirReturnType = ".test";
const validInputDataFolderExists: TRunMkdir = {
  folder: ".test",
};
const validOutputDataFolderExists: TRunMkdirReturnType = undefined;
const validInputDataNested: TRunMkdir = {
  folder: ".test/nested1",
};
const validOutputDataNested: TRunMkdirReturnType = ".test/nested1";
const validInputDataNested2: TRunMkdir = {
  folder: ".test/nested2/nested3",
};
const validOutputDataNested2: TRunMkdirReturnType = ".test/nested2";
const invalidInputData: TRunMkdir = {
  folder:
    "I have a piece of code that I need to test. One of the test requires to add coverage where a stream.Writable is created giving a specified output file name. I need to test the case where the Writable should fail, that it should emit the error event and the proper error handling function should execute, etc. etc.",
};

/**
 * Clean up after the test cases
 */
afterAll(async () => {
  await runRmrf({ folder: ".test" });
});

/**
 * The test cases
 */
it("Fails safely, with an error", async () => {
  const result = await runMkdir(invalidInputData);
  expect(result).toBeInstanceOf(Error);
});

it("Works, Returns string -- the created path -- if `recursive` is `true`", async () => {
  const result = await runMkdir(validInputData);
  expect(result).toEqual(validOutputData);
});

it("Works, Returns `undefined`, when the folder already exists", async () => {
  const result = await runMkdir(validInputDataFolderExists);
  expect(result).toEqual(validOutputDataFolderExists);
});

it("Works, Creates nested folders, one level deep", async () => {
  const result = await runMkdir(validInputDataNested);
  expect(result).toEqual(validOutputDataNested);
});

it("Works, Creates the first nested folder from a structure more than one level deep ", async () => {
  const result = await runMkdir(validInputDataNested2);
  expect(result).toEqual(validOutputDataNested2);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runMkdir({ ...validInputData, isVerbose: true });
  expect(mockLog).toHaveBeenCalled();

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runMkdir();
  expect(result).toBeNull();
});
