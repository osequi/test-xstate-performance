/**
 * Runs the mkdir NodeFS command
 */

import { isEmpty, defaultsDeep, toString } from "lodash";
import { mkdir } from "node:fs/promises";

/**
 * Defines the function interface
 */
export interface TRunMkdir {
  /**
   * The path of the folder to create
   */
  folder?: string;
  /**
   * The mkdir options
   */
  options?: {
    recursive?: boolean;
  };
  /**
   * Logging verbosity
   */
  isVerbose?: boolean;
}

/**
 * Defines the function default props
 */
export const runMkdirDefaultProps: TRunMkdir = {
  folder: "",
  options: {
    recursive: true,
  },
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - null: invalid or missing props
 * - undefined: success, when the folder already exists
 * - string: success, the created path
 * - Error: error
 */
export type TRunMkdirReturnType = null | undefined | string | Error;

/**
 * Defines the function body
 */
export async function runMkdir(
  props?: TRunMkdir
): Promise<TRunMkdirReturnType> {
  const propsMerged = defaultsDeep(props, runMkdirDefaultProps);
  const { folder, options, isVerbose } = propsMerged;

  /**
   * Logs the input props, when isVerbose is true
   */
  if (isVerbose) {
    console.log("Running mkdir:", propsMerged);
  }

  /**
   * Fails safely, with invalid or missing props
   */
  if (isEmpty(folder)) return null;

  /**
   * Runs the mkdir command
   */
  let result = undefined;

  try {
    /**
     * - Returns string -- the created path -- if `recursive` is `true`
     * - Returns `undefined`, when the folder already exists
     * - Creates nested folders, one level deep
     * - Creates the first nested folder from a structure more than one level deep
     *
     * See https://nodejs.org/api/fs.html#fspromisesmkdirpath-options
     */
    result = await mkdir(folder, options);
  } catch (error: unknown) {
    /**
     * Fails safely, with an error
     */
    result = new Error(toString(error));
  }

  return result;
}
