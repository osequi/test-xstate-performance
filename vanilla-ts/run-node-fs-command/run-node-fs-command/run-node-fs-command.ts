/**
 * Runs a set of Node FS commands, and normalizes the result
 */

import { isEmpty, defaultsDeep } from "lodash";
import {
  runAppendfile,
  runMkdir,
  runReadfile,
  runWritefile,
  runRmrf,
} from "../";

/**
 * Defines the function interface
 */
export interface TRunNodeFsCommand {
  /**
   * The command to run
   */
  command: "mkdir" | "readFile" | "writeFile" | "appendFile" | "rmRf";
  /**
   * The file to run the command on
   */
  file?: string;
  /**
   * The folder to run the command on
   */
  folder?: string;
  /**
   * The content to write to the file
   */
  content?: string;
  /**
   * Logging verbosity
   */
  isVerbose?: boolean;
}

/**
 * Defines the function default props
 */
export const runNodeFsCommandDefaultProps: TRunNodeFsCommand = {
  command: "mkdir",
  file: "",
  folder: "",
  content: "",
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - null: invalid or missing props
 * - true: success
 * - string: success, when reading a file
 * - void: required by the readFile result
 * - false: error
 */
export type TRunNodeFsCommandReturnType = null | string | void | boolean;

/**
 * Defines the function body
 */
export async function runNodeFsCommand(
  props?: TRunNodeFsCommand
): Promise<TRunNodeFsCommandReturnType> {
  const propsMerged = defaultsDeep(props, runNodeFsCommandDefaultProps);
  const { command, file, folder, content, isVerbose } = propsMerged;

  /**
   * Logs the input props, when isVerbose is true
   */
  if (isVerbose) {
    console.log("Running the Node FS command:", propsMerged);
  }

  /**
   * Fails safely, with invalid or missing props
   */
  if (isEmpty(folder) && isEmpty(file)) return null;

  /**
   * Runs the command
   */
  let result = undefined;

  switch (command) {
    case "mkdir":
      /**
       * Works, with mkdir
       */
      result = await runMkdir({ folder, isVerbose });
      break;
    case "readFile":
      /**
       * Works, with readFile
       */
      result = await runReadfile({ file, isVerbose });
      break;
    case "writeFile":
      /**
       * Works, with writeFile
       */
      result = await runWritefile({ file, content, isVerbose });
      break;
    case "appendFile":
      /**
       * Works, with appendFile
       */
      result = await runAppendfile({ file, content, isVerbose });
      break;
    case "rmRf":
      /**
       * Works, with rmRf
       */
      result = await runRmrf({ folder, isVerbose });
      break;
  }

  /**
   * Logs the result, when isVerbose is true
   */
  if (isVerbose) {
    console.log(`${command} done: ${result}`);
  }

  /**
   * Normalizes the result
   */
  if (command === "readFile") {
    if (result instanceof Error) {
      /**
       * Returns false instead of an Error, when the readFile result is an Error
       */
      return false;
    } else {
      return result;
    }
  }

  /**
   * Returns true, when the result is not an Error
   */
  return !(result instanceof Error);
}
