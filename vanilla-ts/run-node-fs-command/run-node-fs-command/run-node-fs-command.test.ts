import {
  runNodeFsCommand,
  TRunNodeFsCommand,
  TRunNodeFsCommandReturnType,
} from "./run-node-fs-command";
import { runRmrf } from "../run-rmrf/run-rmrf";

/**
 * Input and output data for testing
 */
const validInputDataMkdir: TRunNodeFsCommand = {
  command: "mkdir",
  folder: ".test",
};
const validOutputData: TRunNodeFsCommandReturnType = true;
const validInputDataReadFile: TRunNodeFsCommand = {
  command: "readFile",
  file: "package.json",
};
const validInputDataWriteFile: TRunNodeFsCommand = {
  command: "writeFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataAppendFile: TRunNodeFsCommand = {
  command: "appendFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataRmRf: TRunNodeFsCommand = {
  command: "rmRf",
  folder: ".test",
};
const invalidInputDataReadFile: TRunNodeFsCommand = {
  command: "readFile",
  file: "invalidFile",
};
const invalidOutputData: TRunNodeFsCommandReturnType = false;

/**
 * Clean up after the test cases
 */
afterAll(async () => {
  await runRmrf({ folder: ".test.writeFile.txt" });
  await runRmrf({ folder: ".test" });
});

/**
 * The test cases
 */
it("Works, with mkdir", async () => {
  const result = await runNodeFsCommand(validInputDataMkdir);
  expect(result).toBe(validOutputData);
});

it("Works, with readFile", async () => {
  const result = await runNodeFsCommand(validInputDataReadFile);
  expect(typeof result).toBe("string");
});

it("Works, with writeFile", async () => {
  const result = await runNodeFsCommand(validInputDataWriteFile);
  expect(result).toBe(validOutputData);
});

it("Works, with appendFile", async () => {
  const result = await runNodeFsCommand(validInputDataAppendFile);
  expect(result).toBe(validOutputData);
});

it("Works, with rmRf", async () => {
  const result = await runNodeFsCommand(validInputDataRmRf);
  expect(result).toBe(validOutputData);
});

it("Works, Returns false instead of an Error, when the readFile result is an Error", async () => {
  const result = await runNodeFsCommand(invalidInputDataReadFile);
  expect(result).toBe(invalidOutputData);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runNodeFsCommand({ ...validInputDataMkdir, isVerbose: true });
  expect(mockLog).toHaveBeenCalled();

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runNodeFsCommand();
  expect(result).toBeNull();
});
