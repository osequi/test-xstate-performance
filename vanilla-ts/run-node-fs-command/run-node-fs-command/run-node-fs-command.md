# runNodeFsCommand

## Description

- See [run-node-fs-command](run-node-fs-command.ts).

## Test coverage

- Run the unit test coverage task: `yarn test:coverage`
- See the [unit test coverage report](../../.coverage/lcov-report/index.html)

## Generated code

- Generated with the function generator version: 0.0.4
