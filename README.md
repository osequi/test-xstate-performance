# test-xstate-performance

Testing speed of execution / benchmarking.

## Notes

- The code does classic, async Node FS operations
- Both versions uses the same amount (the same) tests

## Running tests

```bash
yarn test vanilla-ts/run-node-fs-command/run-node-fs-command/
yarn test xstate/
```

## Benchmarks

```bash
npx ts-node benchmark/benny.ts
npx ts-node benchmark/benny-vanilla.ts
npx ts-node benchmark/benny-xstate.ts
```

## Results

```
Vanilla:
    1 995 ops/s, ±4.17%   | fastest

  Xstate:
    573 ops/s, ±10.88%     | slowest, 71.28% slower
```
