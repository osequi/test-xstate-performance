/**
 * Runs a set of Node FS commands, with Xstate and ADT
 */

import { defaultsDeep } from "lodash";
import {
  createMachine,
  interpret,
  assign,
  MachineConfig,
  MachineOptions,
} from "xstate";
import { waitFor } from "xstate/lib/waitFor";
import { appendFile, mkdir, readFile, rm, writeFile } from "node:fs/promises";
import { Result, Nullable } from "./adt";

/**
 * Defines the function interface
 */
export interface TRunNodeFsCommandAdt2 {
  /**
   * The command to run
   */
  command: "mkdir" | "readFile" | "writeFile" | "appendFile" | "rmRf";
  /**
   * The file to run the command on
   */
  file?: Nullable<string>;
  /**
   * The folder to run the command on
   */
  folder?: Nullable<string>;
  /**
   * The content to write to the file
   */
  content?: Nullable<string>;
  /**
   * Logging verbosity
   */
  isVerbose?: Nullable<boolean>;
}

/**
 * Defines the function default props
 */
export const runNodeFsCommandAdt2DefaultProps: TRunNodeFsCommandAdt2 = {
  command: "mkdir",
  file: undefined,
  folder: undefined,
  content: undefined,
  isVerbose: false,
};

/**
 * Defines the function return type
 *
 * - On success:
 *  - The contents of the Readfile
 *  - Or anything else a node fs call might return
 * - On error:
 *  - The error message
 */
export type TRunNodeFsCommandAdt2ReturnType = Result<
  Nullable<string>,
  Nullable<string>
>;

/**
 * Defines the default function return value
 */
export const runNodeFsCommandAdt2DefaultReturntype: TRunNodeFsCommandAdt2ReturnType =
  {
    ok: false,
    error: undefined,
  };

/**
 * Defines the Xstate schema
 */
interface Schema {
  states: {
    RUNNING: {};
    RETURNING: {};
  };
}

/**
 * Defines the Xstate context
 */
interface Context {
  props: TRunNodeFsCommandAdt2;
  result: TRunNodeFsCommandAdt2ReturnType;
}

/**
 * Defines Xstate transitions
 */
type Transitions = { type: "" };

/**
 * Configures the Xstate machine
 */
const config: MachineConfig<Context, Schema, Transitions> = {
  id: "runNodeFsCommandAdt2",
  context: {} as Context,
  initial: "RUNNING",
  predictableActionArguments: true,
  states: {
    RUNNING: {
      description: "Runs the Node FS command",
      invoke: {
        src: "run",
        onDone: {
          target: "RETURNING",
          actions: "setResultWhenDone",
        },
        onError: {
          target: "RETURNING",
          actions: "setResultWhenError",
        },
      },
    },
    RETURNING: {
      description: "Returns the result",
      entry: "done",
      type: "final",
    },
  },
};

/**
 * Sets up the options for the Xstate machine
 */
export const options: Partial<MachineOptions<Context, any>> = {
  services: {
    run: async (context: Context) => {
      const { props } = context;
      const { command, folder, file, content } = props;
      switch (command) {
        case "mkdir":
          /**
           * NOTE: The Option type has no effect here
           * We still have to typecast ...
           *
           * NOTE: We don't return here a proper result
           * - The result will be asembled later on the onDone and onError events
           * - Also we need no try..catch here, it will be handled by Xstate
           */
          return await mkdir(folder as string, { recursive: true });
        case "readFile":
          return await readFile(file as string, "utf8");
        case "writeFile":
          return await writeFile(file as string, content as string, "utf8");
        case "appendFile":
          return await appendFile(file as string, content as string, "utf8");
        case "rmRf":
          return await rm(folder as string, { recursive: true });
      }
    },
  },
  actions: {
    /**
     * Sets up the result context with event data, after an invoke
     * - See https://xstate.js.org/docs/guides/communication.html#invoking-promises
     */
    setResultWhenDone: assign((context: Context, event) => {
      const { props } = context;
      return { props, result: { ok: true, value: event?.data } };
    }),
    setResultWhenError: assign((context: Context, event) => {
      const { props } = context;
      return { props, result: { ok: false, error: event?.data?.message } };
    }),
    done: assign((context: Context) => {
      return context;
    }),
  },
};

/**
 * Defines the function body
 */
export async function runNodeFsCommandAdt2(
  props?: TRunNodeFsCommandAdt2
): Promise<TRunNodeFsCommandAdt2ReturnType> {
  const propsMerged = defaultsDeep(props, runNodeFsCommandAdt2DefaultProps);
  const { isVerbose } = propsMerged;

  /**
   * Sets up the default result
   */
  let result = runNodeFsCommandAdt2DefaultReturntype;

  /**
   * Sets up the machine context with props
   */
  const configWithProps = {
    ...config,
    context: {
      props: propsMerged,
      result: result,
    },
  };

  /**
   * Sets up and runs the machine
   */
  const machine = createMachine(configWithProps, options);
  const actor = interpret(machine).start();

  /**
   * Gets the results from the machine
   */
  const finalState = await waitFor(actor, (state) => state.done === true);
  result = finalState?.context?.result;

  /**
   * Logs the results
   */
  if (isVerbose) {
    console.log("runNodeFsCommand props:", propsMerged);
    console.log("runNodeFsCommand result:", result);
  }

  return result;
}
