/**
 * Rust-like ADT and PM
 * Using GPT
 * - See https://chat.openai.com/chat
 */

/**
 * Supports for missing / invalid data
 */
export type Nullable<T> = T | undefined;

/**
 * A composite type with two fields:
 * - ok: true / false
 * - value: return value / error object
 */
export type Result<T, E> = { ok: true; value: T } | { ok: false; error: E };

/**
 * Evaluates the result
 */
export function matchResult<T, E, R>(
  result: Result<T, E>,
  handlers: {
    ok: (value: T) => R;
    error: (error: E) => R;
  }
): R {
  if (result.ok) {
    return handlers.ok(result.value);
  } else {
    return handlers.error(result.error);
  }
}
