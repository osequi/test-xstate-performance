import {
  runNodeFsCommandAdt2,
  TRunNodeFsCommandAdt2,
  TRunNodeFsCommandAdt2ReturnType,
} from "./run-node-fs-command-adt-2";
import { matchResult } from "./adt";

/**
 * Input and output data for testing
 */
const validInputDataMkdir: TRunNodeFsCommandAdt2 = {
  command: "mkdir",
  folder: ".test",
};
const validOutputData: TRunNodeFsCommandAdt2ReturnType = {
  ok: true,
  value: ".test",
};
const validInputDataReadFile: TRunNodeFsCommandAdt2 = {
  command: "readFile",
  file: "package.json",
};
const validOutputDataReadFileType = "string";
const validInputDataWriteFile: TRunNodeFsCommandAdt2 = {
  command: "writeFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validOutputDataWriteFile: TRunNodeFsCommandAdt2ReturnType = {
  ok: true,
  value: undefined,
};
const validInputDataAppendFile: TRunNodeFsCommandAdt2 = {
  command: "appendFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataDeleteFile: TRunNodeFsCommandAdt2 = {
  command: "rmRf",
  file: ".test.writeFile.txt",
};
const validInputDataRmRf: TRunNodeFsCommandAdt2 = {
  command: "rmRf",
  folder: ".test",
};
const invalidInputDataReadFile: TRunNodeFsCommandAdt2 = {
  command: "readFile",
  file: "invalidFile",
};
const invalidOutputData: TRunNodeFsCommandAdt2ReturnType = {
  ok: false,
  error: "ENOENT: no such file or directory, open 'invalidFile'",
};
const invalidOutputDataNoProps: TRunNodeFsCommandAdt2ReturnType = {
  ok: false,
  error:
    'The "path" argument must be of type string or an instance of Buffer or URL. Received undefined',
};

/**
 * Clean up after the test cases
 */
beforeAll(async () => {
  await runNodeFsCommandAdt2(validInputDataRmRf);
  await runNodeFsCommandAdt2(validInputDataDeleteFile);
});

/**
 * The test cases
 */
it("Works, with mkdir", async () => {
  const result = await runNodeFsCommandAdt2(validInputDataMkdir);
  expect(result).toStrictEqual(validOutputData);
});

it("Works, with readFile", async () => {
  const result = await runNodeFsCommandAdt2(validInputDataReadFile);
  const result2 = matchResult(result, {
    ok: (value) => value,
    error: () => null,
  });
  expect(typeof result2).toEqual(validOutputDataReadFileType);
});

it("Works, with writeFile", async () => {
  const result = await runNodeFsCommandAdt2(validInputDataWriteFile);
  expect(result).toStrictEqual(validOutputDataWriteFile);
});

it("Works, with appendFile", async () => {
  const result = await runNodeFsCommandAdt2(validInputDataAppendFile);
  expect(result).toStrictEqual(validOutputDataWriteFile);
});

it("Works, with rmRf", async () => {
  const result = await runNodeFsCommandAdt2(validInputDataRmRf);
  expect(result).toStrictEqual(validOutputDataWriteFile);
});

it("Works, Returns false instead of an Error, when the readFile result is an Error", async () => {
  const result = await runNodeFsCommandAdt2(invalidInputDataReadFile);
  expect(result).toStrictEqual(invalidOutputData);
});

it("Works, Logs the input props, when isVerbose is true", async () => {
  const mockLog = jest.spyOn(console, "log").mockImplementation((string) => {
    return string;
  });

  await runNodeFsCommandAdt2({ ...validInputDataMkdir, isVerbose: true });
  expect(mockLog).toHaveBeenCalled();

  mockLog.mockRestore();
});

it("Fails safely, with invalid or missing props", async () => {
  const result = await runNodeFsCommandAdt2();
  expect(result).toStrictEqual(invalidOutputDataNoProps);
});
