# runNodeFsCommandAdt2

## Description

- See [run-node-fs-command-adt-2](run-node-fs-command-adt-2.ts).

## Test coverage

- Run the unit test coverage task: `yarn test:coverage`
- See the [unit test coverage report](../../.coverage/lcov-report/index.html)

## Generated code

- Generated with the function generator version: Not found
