import * as b from "benny";
import {
  runNodeFsCommand,
  TRunNodeFsCommand,
  TRunNodeFsCommandReturnType,
} from "../vanilla-ts/run-node-fs-command";
import {
  runNodeFsCommandAdt2,
  TRunNodeFsCommandAdt2,
  TRunNodeFsCommandAdt2ReturnType,
} from "../xstate";
import { matchResult } from "../xstate/adt";

/**
 * Input and output data for testing
 */
const validInputDataMkdir: TRunNodeFsCommand = {
  command: "mkdir",
  folder: ".test",
};
const validOutputData: TRunNodeFsCommandReturnType = true;
const validInputDataReadFile: TRunNodeFsCommand = {
  command: "readFile",
  file: "package.json",
};
const validInputDataWriteFile: TRunNodeFsCommand = {
  command: "writeFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataAppendFile: TRunNodeFsCommand = {
  command: "appendFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataRmRf: TRunNodeFsCommand = {
  command: "rmRf",
  folder: ".test",
};
const invalidInputDataReadFile: TRunNodeFsCommand = {
  command: "readFile",
  file: "invalidFile",
};
const invalidOutputData: TRunNodeFsCommandReturnType = false;

const validInputDataMkdirAdt2: TRunNodeFsCommandAdt2 = {
  command: "mkdir",
  folder: ".test",
};
const validOutputDataAdt2: TRunNodeFsCommandAdt2ReturnType = {
  ok: true,
  value: ".test",
};
const validInputDataReadFileAdt2: TRunNodeFsCommandAdt2 = {
  command: "readFile",
  file: "package.json",
};
const validOutputDataReadFileTypeAdt2 = "string";
const validInputDataWriteFileAdt2: TRunNodeFsCommandAdt2 = {
  command: "writeFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validOutputDataWriteFileAdt2: TRunNodeFsCommandAdt2ReturnType = {
  ok: true,
  value: undefined,
};
const validInputDataAppendFileAdt2: TRunNodeFsCommandAdt2 = {
  command: "appendFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataDeleteFileAdt2: TRunNodeFsCommandAdt2 = {
  command: "rmRf",
  file: ".test.writeFile.txt",
};
const validInputDataRmRfAdt2: TRunNodeFsCommandAdt2 = {
  command: "rmRf",
  folder: ".test",
};
const invalidInputDataReadFileAdt2: TRunNodeFsCommandAdt2 = {
  command: "readFile",
  file: "invalidFile",
};
const invalidOutputDataAdt2: TRunNodeFsCommandAdt2ReturnType = {
  ok: false,
  error: "ENOENT: no such file or directory, open 'invalidFile'",
};
const invalidOutputDataNoPropsAdt2: TRunNodeFsCommandAdt2ReturnType = {
  ok: false,
  error:
    'The "path" argument must be of type string or an instance of Buffer or URL. Received undefined',
};

b.suite(
  "All",

  b.add("Vanilla", async () => {
    await runNodeFsCommand(validInputDataMkdir);
    await runNodeFsCommand(validInputDataReadFile);
    await runNodeFsCommand(validInputDataWriteFile);
    await runNodeFsCommand(validInputDataAppendFile);
    await runNodeFsCommand(validInputDataRmRf);
    await runNodeFsCommand(invalidInputDataReadFile);
    await runNodeFsCommand();
  }),

  b.add("Xstate", async () => {
    await runNodeFsCommandAdt2(validInputDataMkdirAdt2);
    await runNodeFsCommandAdt2(validInputDataReadFileAdt2);
    await runNodeFsCommandAdt2(validInputDataWriteFileAdt2);
    await runNodeFsCommandAdt2(validInputDataAppendFileAdt2);
    await runNodeFsCommandAdt2(validInputDataRmRfAdt2);
    await runNodeFsCommandAdt2(invalidInputDataReadFileAdt2);
    await runNodeFsCommandAdt2();
  }),

  b.cycle(),
  b.complete(),
  b.save({ file: "reduce", version: "1.0.0" }),
  b.save({ file: "reduce", format: "chart.html" })
);
