import * as b from "benny";
import {
  runNodeFsCommand,
  TRunNodeFsCommand,
  TRunNodeFsCommandReturnType,
} from "../vanilla-ts/run-node-fs-command";

/**
 * Input and output data for testing
 */
const validInputDataMkdir: TRunNodeFsCommand = {
  command: "mkdir",
  folder: ".test",
};
const validOutputData: TRunNodeFsCommandReturnType = true;
const validInputDataReadFile: TRunNodeFsCommand = {
  command: "readFile",
  file: "package.json",
};
const validInputDataWriteFile: TRunNodeFsCommand = {
  command: "writeFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataAppendFile: TRunNodeFsCommand = {
  command: "appendFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataRmRf: TRunNodeFsCommand = {
  command: "rmRf",
  folder: ".test",
};
const invalidInputDataReadFile: TRunNodeFsCommand = {
  command: "readFile",
  file: "invalidFile",
};
const invalidOutputData: TRunNodeFsCommandReturnType = false;

b.suite(
  "Vanilla TS",

  b.add("Run all commands from test", async () => {
    await runNodeFsCommand(validInputDataMkdir);
    await runNodeFsCommand(validInputDataReadFile);
    await runNodeFsCommand(validInputDataWriteFile);
    await runNodeFsCommand(validInputDataAppendFile);
    await runNodeFsCommand(validInputDataRmRf);
    await runNodeFsCommand(invalidInputDataReadFile);
    await runNodeFsCommand();
  }),

  b.cycle(),
  b.complete(),
  b.save({ file: "reduce-vanilla", version: "1.0.0" }),
  b.save({ file: "reduce-vanilla", format: "chart.html" })
);
