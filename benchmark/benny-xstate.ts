import * as b from "benny";

import {
  runNodeFsCommandAdt2,
  TRunNodeFsCommandAdt2,
  TRunNodeFsCommandAdt2ReturnType,
} from "../xstate";
import { matchResult } from "../xstate/adt";

/**
 * Input and output data for testing
 */
const validInputDataMkdir: TRunNodeFsCommandAdt2 = {
  command: "mkdir",
  folder: ".test",
};
const validOutputData: TRunNodeFsCommandAdt2ReturnType = {
  ok: true,
  value: ".test",
};
const validInputDataReadFile: TRunNodeFsCommandAdt2 = {
  command: "readFile",
  file: "package.json",
};
const validOutputDataReadFileType = "string";
const validInputDataWriteFile: TRunNodeFsCommandAdt2 = {
  command: "writeFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validOutputDataWriteFile: TRunNodeFsCommandAdt2ReturnType = {
  ok: true,
  value: undefined,
};
const validInputDataAppendFile: TRunNodeFsCommandAdt2 = {
  command: "appendFile",
  file: ".test.writeFile.txt",
  content: "test",
};
const validInputDataDeleteFile: TRunNodeFsCommandAdt2 = {
  command: "rmRf",
  file: ".test.writeFile.txt",
};
const validInputDataRmRf: TRunNodeFsCommandAdt2 = {
  command: "rmRf",
  folder: ".test",
};
const invalidInputDataReadFile: TRunNodeFsCommandAdt2 = {
  command: "readFile",
  file: "invalidFile",
};
const invalidOutputData: TRunNodeFsCommandAdt2ReturnType = {
  ok: false,
  error: "ENOENT: no such file or directory, open 'invalidFile'",
};
const invalidOutputDataNoProps: TRunNodeFsCommandAdt2ReturnType = {
  ok: false,
  error:
    'The "path" argument must be of type string or an instance of Buffer or URL. Received undefined',
};

b.suite(
  "Xstate",

  b.add("Run all commands from test", async () => {
    await runNodeFsCommandAdt2(validInputDataMkdir);
    await runNodeFsCommandAdt2(validInputDataReadFile);
    await runNodeFsCommandAdt2(validInputDataWriteFile);
    await runNodeFsCommandAdt2(validInputDataAppendFile);
    await runNodeFsCommandAdt2(validInputDataRmRf);
    await runNodeFsCommandAdt2(invalidInputDataReadFile);
    await runNodeFsCommandAdt2();
  }),

  b.cycle(),
  b.complete(),
  b.save({ file: "reduce-xstate", version: "1.0.0" }),
  b.save({ file: "reduce-xstate", format: "chart.html" })
);
